const express = require('express');
const cors = require('cors');
const helmet = require('helmet'); 
const sequelize = require('../database/connection')
require('dotenv').config();
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const swaggerOptions = require('../utils/swaggerOptions');
const session = require('express-session');
const passport = require('passport');
const mercadopago = require('mercadopago');

mercadopago.configure({
    access_token: 'TEST-8100824363836021-113001-d70df353566e4c3619bd4b2e387e68f6-1029428182'
});


const EXPRESSPORT = process.env.EXPORT || 3000;

//Swagger Documentacion
const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

//Rutas
const routes = require('../routes');
require('../models/associations');

//uso Cors
app.use(cors());

//helmet
app.use(helmet());

//Express session
let sess = {
    secret: 'CHANGE THIS TO A RANDOM SECRET',
    cookie: {},
    resave: false,
    saveUninitialized: true
  };

app.use(session(sess));

//Passport
app.use(passport.initialize());
app.use(passport.session());

//Body Parse
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.use('/', routes);

app.listen(EXPRESSPORT, ()=>{ 
    console.log(`server listening on port ${EXPRESSPORT}`);

    //conexion a la BD
    sequelize.sync({alter:false}).then(()=>{
        console.log("nos hemos conectado a la BD");
    }).catch(error=>{
        console.error("se ha presentado un error", error)
    })
});