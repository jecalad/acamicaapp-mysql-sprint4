const Pedidos = require('../models/pedidos.model');

async function cerrarPedido(id){
    const pedido = await Pedidos.findOne({where: {id: id}});
    if(!pedido) throw "pedido no encontrado";
    const estado = { estado : "cerrado"}; 
    try{
        await Pedidos.update(estado, {where:{id:id}})
    }catch(err){
        throw "no se pudo actualizar"
    }
}

module.exports = { cerrarPedido };