//variables de entorno
require('dotenv').config();
const request = require('request');

const CLIENT = process.env.PAYPAL_CLIENT_ID;
const SECRET = process.env.PAYPAL_SECRET;
const PAYPAL_API = 'https://api-m.sandbox.paypal.com';
const auth = { user: CLIENT, pass: SECRET};

async function createPayment(req, res){

    const { value } = req.params;

    const body = {
        intent: 'CAPTURE',
        purchase_units: [{
            amount: {
                currency_code: 'USD',
                value: value
            }
        }],
        application_context: {
            brand_name: 'Delilah',
            landing_page: 'NO_PREFERENCE',
            user_action: 'PAY_NOW',
            return_url: 'http://localhost:3000/paypal/execute-payment',
            cancel_url: 'http:localhost:3000/paypal/cancel-payment'
        }
    }
    
    request.post(`${PAYPAL_API}/v2/checkout/orders`,{
        auth,
        body,
        json: true
    }, (err, response) =>{
        res.json({ data: response.body});
    });
    
};


// Capturar el dinero -- Aprovar orden
async function executePayment(req, res){
    const token = req.query.token;
    console.log(token);

    request.post(`${PAYPAL_API}/v2/checkout/orders/${token}/capture`, {
        auth,
        body: {},
        json:true
    }, (err, response)=>{
        
        res.send(
            `
                <h2> Payment Result </h2>
                <h3> ${response.body.status} </h3>
                <hr>
                <h4> Order ID: ${response.body.id} </h4><br>
                <h4> Payer : ${response.body.payer.name.given_name} ${response.body.payer.name.surname} </h4>
            `
        );
    });
}

module.exports = { createPayment, executePayment };