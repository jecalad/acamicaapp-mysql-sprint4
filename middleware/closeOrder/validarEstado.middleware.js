const Pedidos = require('../../models/pedidos.model');
const Perfiles = require('../../models/perfil.models');

const jwt = require('jsonwebtoken');
require('dotenv').config();



async function validarEstado(req, res, next) {
    const { id } = req.params;

    try{
        const token = req.query.token;
        const validateToken = jwt.verify(token, process.env.JWT_SECRET);
        const userId = validateToken.user_id;

        const pedido = await Pedidos.findByPk(id);
        if(!pedido) throw res.status(500).json({"error": "Pedido no encontrado"});

        if(pedido.estado != "pidiendo") return res.status(500).json({"error": "No se puede actualizar estado"});

        //Validar Usuario quien cierra el pedido
        if(pedido.usuario_id != userId) return "este usuario no puede cerrar el pedido con id " + id; 

        //validar que el pedido tenga una direccion
        if(pedido.direccionxenvio == null || pedido.direccionxenvio == '' || pedido.direccionxenvio == undefined){
            const perfil = await Perfiles.findOne({where:{user_id:userId}});
            if(perfil.direccion == null) return res.status(500).json({error:"el pedido no tiene direcciond de envio"});
            await Pedidos.update({direccionxenvio: perfil.direccion}, {where: {usuario_id:userId}})
        }
    next();
    }catch(err){
        return res.status(500).json(err);
    }
    
}

module.exports = { validarEstado };