const jwt = require('jsonwebtoken');
require('dotenv').config();

function validarToken(req, res, next){

    if (req.isAuthenticated()){
        const token = req.query.token;
        try{
            const gettoken = jwt.verify(token, process.env.JWT_SECRET);
            if(!gettoken.activo) return res.status(403),json({error: "usuario inactivo"});
              return next();
          }catch(err){
              return res.status(403).json({ status: "error", error: err});
          }
    }

    return res.status(401).json({ status: "error", error: "no autenticado"});
 
    
}

module.exports = {validarToken};