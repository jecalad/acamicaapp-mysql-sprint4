const express = require('express');
const router = express.Router();
const mercadoPago = require('mercadopago');

router.post('/payment', async (req, res)=>{
    const paymentData = req.body;

    try{
        const response = await mercadoPago.payment.create(paymentData);
        res.json({ id: response.body.id, status: response.body.status });
    }catch(err){
   
        console.log(err);
        res.status(500).json(err);
    }
});

module.exports = router;