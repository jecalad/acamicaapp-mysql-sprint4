const express = require('express');
const router = express.Router();
const {createPayment, executePayment} = require('../controllers/paypal.controller');

router.post('/create-payment/:value', async (req, res)=>{
    await createPayment(req, res);
})

router.get('/cancel-payment', (req, res)=>{
    res.status(401).send("<h1> ORDER CANCELLED</h1>");
});

router.get('/execute-payment', async (req,res)=>{
    await  executePayment(req, res);
});


module.exports = router;