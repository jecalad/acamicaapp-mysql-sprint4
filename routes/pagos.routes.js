const express = require('express');
const router = express.Router();
const {listarMetodosPago, crearMetodoPago, listarUnMetodoPago, borrarPago, actualizarPago} = require('../controllers/pagos.controller');
const {validateAdmin} = require('../middleware/usuarios/validateAdmin.middleware');
const {validarToken} = require('../middleware/login/login.middleware');

/**
 *@swagger
 * /pagos:
 *  get:
 *      summary: Returns users list of payment methods
 *      tags: [Pagos]
 *      responses:
 *          200:
 *              description: return list of payment methods
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/listpayment'
 *          401:
 *              description: authentication required
 */

//Get all payment methods
router.get('/', async (req, res)=>{

    try{
        const metodos = await listarMetodosPago();
        res.status(200).json(metodos);
    }catch(err){const express = require('express');
    const router = express.Router();
        res.status(500).json({error: err});
    }
})


/**
 *@swagger
 * /pagos/{id}:
 *  get:
 *      summary: Returns specific payment method
 *      tags: [Pagos]
 *      parameters:
 *          -   in: path
 *              name: id
 *      responses:
 *          200:
 *              description: return specific payment method from database
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/listpayment'
 *          500:
 *              description: error
 */
router.get('/:id', async (req, res)=>{
    const { id } = req.params;

    try{
        const pago = await listarUnMetodoPago(id);
        res.status(200).json(pago);
    }catch(err){
        res.status(500).json(err);
    }
});


/**
 *@swagger
 * /pagos:
 *  post:
 *      summary: create payment method
 *      tags: [Pagos]
 *      parameters:
 *          -   in: query
 *              name: token
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/listpayment'
 *      responses:
 *          200:
 *              description: return list of users in the database
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/listUsers'
 *          500:
 *              description: se ha producido un error
 */
router.post('/', validateAdmin, validarToken, async (req, res)=>{
    const { tipo } = req.body;
    
    try{
        const pago = await crearMetodoPago(tipo);
        res.status(200).json(pago);
    }catch(err){
        res.status(500).json({"error": err});
    }
});


/**
 *@swagger
 * /pagos/{id}:
 *  put:
 *      summary: update payment method
 *      tags: [Pagos]
 *      parameters:
 *          -   in: path
 *              name: id
 *          -   in: query
 *              name: token
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/updatepayment'
 *      responses:
 *          200:
 *              description: payment method was updated successfully
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          500:
 *              description: se ha producido un error
 */
router.put('/:id', validateAdmin, validarToken, async (req, res)=>{
    const { tipo } = req.body;
    const { id } = req.params;

    try{
        await actualizarPago(tipo, id);
        res.status(200).json({message: "Método de pago actualizado"});
    }catch(err){
        res.status(500).json(err);
    }
});

/**
 *@swagger
 * /pagos/{id}:
 *  delete:
 *      summary: delete specific payment method
 *      tags: [Pagos]
 *      parameters:
 *          -   in: path
 *              name: id
 *          -   in: query
 *              name: token
 *              type: string
 *      responses:
 *          200:
 *              description: payment method was deleted successfully
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          500:
 *              description: se ha producido un error
 */
router.delete('/:id', validateAdmin, validarToken, async (req, res)=>{
    const { id } = req.params;

    try{
        await borrarPago(id);
        res.status(200).json({ message: "Método de pago eliminado"})
    }catch(err){
        res.status(500).json(err);
    }
});

module.exports = router;

/**
 *@swagger
 * tags:
 *  name: Pagos
 *  description: Pagos Section
 * components:
 *  schemas:
 *      listpayment:
 *          type: object
 *          properties:
 *              id:
 *                  type: string
 *                  description: user id
 *              tipo:
 *                  type: string
 *                  description: Payment methods
 *          example:
 *              id: 1
 *              tipo: efectivo
 *      updatepayment:
 *          type: object
 *          required:
 *              - tipo
 *          properties:
 *              tipo:
 *                  type: string
 *                  description: páyment method type
 */