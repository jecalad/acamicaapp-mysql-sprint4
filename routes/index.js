const router = require('express').Router();


const UserRoutes = require('./usuarios.routes');
const PerfilRoutes = require('./perfil.routes');
const ProductsRoutes = require('./productos.routes');
const PedidosRoutes = require('./pedidos.routes');
const LoginRoutes = require('./login.routes');
const MetodosPagoRoutes = require('./pagos.routes');
const closeOrderRoutes = require('./closeOrder.routes');
const googleAuthRouter = require('./auth/google');
const auth0Router = require('./auth/auth0');
const mercadoPago = require('./mercadopago');
const payPalRoutes = require('./payPal.routes');

//middleware
const {validarToken} = require('../middleware/login/login.middleware');

//login
router.use('/login', LoginRoutes);

// list, create , update and delete users
router.use('/usuarios', UserRoutes);

//update user profile only
router.use('/perfil', validarToken, PerfilRoutes);

//list, create, update and delete Products
router.use('/productos', ProductsRoutes);

//create orders
router.use('/pedidos', validarToken, PedidosRoutes);

//crear, read,update and delete Payment Methods
router.use('/pagos',  MetodosPagoRoutes);

//cerrar ordenes
router.use('/closeOrder', closeOrderRoutes);

//Google Auth
router.use('/login/google', googleAuthRouter);

//Login Auth0
router.use('/login/auth0', auth0Router);

//MercadoPago
router.use('/mercadopago', mercadoPago);

//PayPal
router.use('/paypal', payPalRoutes)



module.exports = router;
