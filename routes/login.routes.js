const express = require('express');
const router = express.Router();
const { usuarioLogin } = require('../controllers/login.controller');

/**
 *@swagger
 * /login:
 *  post:
 *      summary: Users login to generate an JWT token
 *      tags: [Login]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/login'
 *      responses:
 *          200:
 *              description: Login succesful
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          500:
 *              description: se ha producido un error
 */

router.post('/',  async (req, res)=>{
    const {nombre_usuario, contrasena} = req.body;

    try{
        const login = await usuarioLogin(nombre_usuario, contrasena);
        return res.status(200).json({message: "login correcto", token: login});
    }catch(err){
        return res.status(500).json({"error": err});
    }
    
});

module.exports = router;

/**
 *@swagger
 * tags:
 *  name: Login
 *  description: Log in Section
 * components:
 *  schemas:
 *      login:
 *          type: object
 *          properties:
 *              nombre_usuario:
 *                  type: string
 *                  description: user id
 *              contrasena:
 *                  type: string
 *                  description: user first name
 *          example:
 *              nombre_usuario: jecalad
 *              contrasena: '0123456789'
 */