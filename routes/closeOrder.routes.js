const express = require('express');
const router = express.Router();

//Middlewares
const { validarEstado } = require('../middleware/closeOrder/validarEstado.middleware');

const { cerrarPedido } = require('../controllers/closeOrder.controller');

router.post('/:id', validarEstado, async (req, res)=>{
    const { id } = req.params;

    try{
        await cerrarPedido(id);
        res.status(200).json({message: "pedido cerrado"})
    }catch(err){
        res.status(500).send(err);
    }
    
});


module.exports = router;