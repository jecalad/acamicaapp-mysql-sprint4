const express = require('express')
// Load Passport
var passport = require('passport');
var Auth0Strategy = require('passport-auth0');

//controller Crear usuario
const { createUser, listarUsuarioEmail } = require('../../controllers/usuarios.controller');

const router = express.Router();

// You can use this section to keep a smaller payload
passport.serializeUser(function (user, done) {
    done(null, user);
});
  
passport.deserializeUser(function (user, done) {
    done(null, user);
});

const strategy = new Auth0Strategy(
    {
      domain: process.env.AUTH0_DOMAIN,
      clientID: process.env.AUTH0_CLIENT_ID,
      clientSecret: process.env.AUTH0_CLIENT_SECRET,
      callbackURL:
        process.env.AUTH0_CALLBACK_URL || 'http://localhost:3000/login/auth0/callback'
    },
    async function (accessToken, refreshToken, extraParams, profile, done) {
      // accessToken is the token to call Auth0 API (not needed in the most cases)
      // extraParams.id_token has the JSON Web Token
      // profile has all the information from the user
      console.log("Profile: ", profile);
      try{
        const checkUser = await listarUsuarioEmail(profile.emails[0].value)
        if(!checkUser){
          await createUser(profile.name.givenName, profile.name.familyName, profile.emails[0].value, profile.nickname);
        }
        
      }catch(err){
        return done(err);
      }
      
      return done(null, profile);
    }
);
  
passport.use(strategy);

router.get('/auth', passport.authenticate('auth0', {
    scope: 'openid email profile'
        }), function (req, res) {
                res.redirect('/usuarios/user');
});
  
router.get('/callback', function (req, res, next) {
passport.authenticate('auth0', function (err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/login'); }
    req.logIn(user, function (err) {
    if (err) { return next(err); }
    const returnTo = req.session.returnTo;
    delete req.session.returnTo;
    res.redirect(returnTo || '/usuarios/user');
    });
})(req, res, next);
});

// Perform session logout and redirect to homepage
router.get('/logout', (req, res) => {
    req.logout();
  
    var returnTo = req.protocol + '://' + req.hostname;
    var port = req.connection.localPort;
    if (port !== undefined && port !== 80 && port !== 443) {
      returnTo += ':' + port;
    }
    var logoutURL = new url.URL(
      util.format('https://%s/v2/logout', process.env.AUTH0_DOMAIN)
    );
    var searchString = querystring.stringify({
      client_id: process.env.AUTH0_CLIENT_ID,
      returnTo: returnTo
    });
    logoutURL.search = searchString;
  
    res.redirect(logoutURL);
    });
  
 

module.exports = router