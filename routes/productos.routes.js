const express = require('express');
const router = express.Router();
const {listarProductos, crearProducto, actualizarProducto, borrarProducto, listarUnProducto} = require('../controllers/productos.controller');
const { validarActualizacionProductos } = require('../middleware/productos/updateProductos.middleware');
const { validateAdmin } = require('../middleware/usuarios/validateAdmin.middleware');
const {validarToken} = require('../middleware/login/login.middleware');

/**
 *@swagger
 * /productos:
 *  get:
 *      summary: Returns product list
 *      tags: [Productos]
 *      responses:
 *          200:
 *              description: return list of products in the database
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/listproducts'
 *          401:
 *              description: authentication required
 *          500:
 *              description: Se ha producido un error
 */
router.get('/', async (req, res)=>{
    try{
        const productos = await listarProductos();
        res.status(200).json(productos);
    }catch(err){
        res.status(500).json({"error": err})
    }
});

/**
 *@swagger
 * /productos/{id}:
 *  get:
 *      summary: Returns specific products by Id
 *      tags: [Productos]
 *      parameters:
 *          -   in: path
 *              name: id
 *      responses:
 *          200:
 *              description: return specific product from database
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/listproduct'
 *          500:
 *              description: error
 */
router.get('/:id', async (req, res)=>{
    const { id } = req.params;
    try{
        const producto = await listarUnProducto(id);
        res.status(200).json(producto);
    }catch(err){
        res.status(500).json({"error": err})
    }
})

/**
 *@swagger
 * /productos:
 *  post:
 *      summary: create product
 *      tags: [Productos]
 *      parameters:
 *          -   in: query
 *              name: token
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/listproducts'
 *      responses:
 *          200:
 *              description: return list of users in the database
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/listproducts'
 *          500:
 *              description: se ha producido un error
 */
router.post('/', validateAdmin, validarToken, async (req, res)=>{
    const { nombre, categoria, precio, stock, seccion } = req.body;

    try{
        const pdtoCreado = await crearProducto(nombre, categoria, precio, stock, seccion);
        res.status(201).json(pdtoCreado);
    }catch(err){
        res.status(500).json({ message: err});
    }
})

/**
 *@swagger
 * /productos/{id}:
 *  patch:
 *      summary: update products
 *      tags: [Productos]
 *      parameters:
 *          -   in: path
 *              name: id
 *          -   in: query
 *              name: token
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/listproducts'
 *      responses:
 *          200:
 *              description: user was updated successfully
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          500:
 *              description: se ha producido un error
 */
router.patch('/:id', validateAdmin, validarActualizacionProductos, validarToken, async (req, res) =>{
    const { id } = req.params;

    try{
        await actualizarProducto(res.productoActualizar, id);
        res.status(201).json({ message: "Producto actualizado"})
    }catch(err){
        res.status(500).json({ message: err});
    }

});

/**
 *@swagger
 * /productos/{id}:
 *  delete:
 *      summary: delete specific product
 *      tags: [Productos]
 *      parameters:
 *          -   in: path
 *              name: id
 *          -   in: query
 *              name: token
 *              type: string
 *      responses:
 *          200:
 *              description: products was deleted successfully
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          500:
 *              description: se ha producido un error
 */
router.delete('/:id', validateAdmin, validarToken, async (req, res) =>{
    const { id } = req.params;

    try{
        await borrarProducto(id);
        res.status(200).json({ message: "Producto eliminado"});
    }catch(err){
        res.status(500).json({ message: err});
    }
});

module.exports = router;

/**
 *@swagger
 * tags:
 *  name: Productos
 *  description: Productos section
 * components:
 *  schemas:
 *      listproducts:
 *          type: object
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: name of the product
 *              categoria:
 *                  type: string
 *                  description: product category
 *              precio:
 *                  type: number
 *                  description: product price
 *              stock:
 *                  type: number
 *                  description: numer of products in stock
 *              seccion:
 *                  type: string
 *                  description: section of what the product belongs
 *          example:
 *              nombre: Suculenta tigre
 *              categoria: suculentas
 *              precio: 10000
 *              stock: 10
 *              seccion: plantas
 *      createUser:
 *          type: object
 *          required:
 *              - nombre
 *              - apellido
 *              - email
 *              - nombre_usuario
 *              - contrasena
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: user first name
 *              apellido:
 *                  type: string
 *                  description: user last name
 *              email:
 *                  type: string
 *                  description: user email
 *              nombre_usuario:
 *                  type: string
 *                  description: user username for system authentication
 *              contrasena:
 *                  type: string
 *                  description: user password for system authentication
 *              direccion:
 *                  type: string
 *                  description: user home address
 *              pais:
 *                  type: string
 *                  description: user country of residence
 *              telefono:
 *                  type: number
 *                  description: user phone number
 */